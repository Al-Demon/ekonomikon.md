# Proyecto traducciones y curso economías alternativas

Proyecto abierto y libre sobre el aprovechamiento y formación en el conocimiento de la existencia de economías alternativas. Con el descubrimiento del libro EKONOMIKON muchas mentes han sido abiertas y ahora son soberanas económicamen, participan de grupos de consumo, cooperativas integrales con monedas sociales y creemos que podemos ir más allá con la difusión de estos conocimientos.


## Objetivos:
    - Difusión de EKONOMIKON
    - Traducción a otros idiomas (actualmente solo en español)
    - Ha sido pasado a Markdown para facilitar sus traducciones e inclusiones en sistemas web.
    - Creación de curso sobre economías alternativas en modo MOOC.
    - Posibles Reediciones en papel.
    - Posible web que recoja todos estos trabajos... de momento en https://gitlab.com/Al-Demon/ekonomikon.md
    
    
    
## Medios de contribución:
- Cualquiera puede utilizar, agregar y modificar cualquier parte de este proyecto siempre que se respete la licencia de creative commons BY SA 4.0 que obliga a que todo el contenido modificado siga teniendo esa licencia por lo que siga siendo libre.

- Por favor incluir ideas y otras vias de aprendizaje.
- Iremos mejorando este proyecto según vayamos avanzando entre todas y para todas.


Cualquier consulta el correo inicial al-demon@3toques.es